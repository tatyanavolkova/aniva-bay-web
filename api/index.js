import axios from "axios"

const api = axios.create({
  baseURL: 'http://api.aniva-bay.d'
})

export default api
